# jtool template

## How to use
```bash
yum install -y rpm-build
rpm --prefix /myopt -ivh ./out5/myapp-1.0-1.x86_64.rpm

```

```bash
JAR_FILE=$(basename $(ls target/*jar-with-dependencies.jar | head -n 1))
jpackage --input target/ --name MyApp --main-jar $JAR_FILE --main-class org.b1b.Main --type app-image --dest out
```

FROM maven:3.9.8-amazoncorretto-21
ENV PATH=$PATH:/usr/lib/jvm/java-21-amazon-corretto/bin

RUN yum install -y binutils

WORKDIR /app
COPY pom.xml /app
COPY src /app/src
RUN mvn clean package

CMD ["/bin/bash"]